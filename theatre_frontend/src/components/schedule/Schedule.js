import React, {Component} from 'react'
import "./schedule.css"
import styles from "../performance_composition/styles";

class Schedule extends Component {
    QUOTE_SERVICE_PERFORMANCE_URL = 'http://localhost:8081/api/repertoire';
    QUOTE_SERVICE_SCHEDULE_URL = 'http://localhost:8081/api/schedule?id=';
    constructor() {
        super();
        this.state = { performances: null, selectedPerformance: null};
    }

    componentDidMount() {
        this.fetchQuotes();
    }

    getScheduleForMentionedPerformance(e) {
        if(e.target.value == 0) {
            this.state = { performances: null, selectedPerformance: null};
        }


        fetch(this.QUOTE_SERVICE_SCHEDULE_URL + e.target.value)
            .then(response => response.json())
            .then(result => this.setState({schedule: result}))
            .catch(e => console.log(e));
    }

    render() {
        return (
            <div className='Repertoire' style={styles.TextWeight}>
                <h2> Расписание спектаклей </h2>
                <h3 style={styles.Info}> Выберите спектакль </h3>
                <select onChange={this.getScheduleForMentionedPerformance.bind(this)}>
                    return <option value={0}>{"-"}</option>
                    {this.state.performances && this.state.performances.map((performance) => {
                        return <option key={performance.id} value={performance.id}>{performance.name}</option>})}
                </select>

                {this.state.selectedPerformance && this.state.selectedPerformance.schedules.map((sessionDataTime) =>{
                    console.log(sessionDataTime);
                    return <h5>{sessionDataTime.sessionDateTime}</h5>;
                })}
                {this.state.schedule && this.state.schedule.map((el) => {
                        return <p>{el}</p>;
                })}
            </div>

        );
    }

    fetchQuotes = () => {
        this.setState({isFetching: true});
        fetch(this.QUOTE_SERVICE_PERFORMANCE_URL)
            .then(response => response.json())
            .then(result => this.setState({performances: result}))
            .catch(e => console.log(e));

    }
}

export default Schedule