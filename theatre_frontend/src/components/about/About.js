import React, {Component} from 'react'
import styles from "./styles.js"
import "./about.css"

class About extends Component {
    render() {
        return(
            <p>
                <h2 style={styles.Header}> Информация о театре </h2>
                <h4 style= {styles.Info} >Новосибирский академический молодёжный театр «Глобус» — один из старейших театров Новосибирска, основан в 1930 году. Имеет широкий
                    разножанровый репертуар, постановки различных режиссёрских школ. Располагается в
                    здании, стилизованном под парусное судно.</h4>
            </p>
        );
    }

}

export default About