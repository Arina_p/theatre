const Header = {
    textAlign: "center",
    textColor: "black",
    fontWeight: 'bold',
    fontSize: "20px"
}

const Info = {
    textAlign: "center",
    textColor: "black",
    fontWeight: 'normal',
    fontSize: "16px"
}

const TextHeight = {
    height : "20px"
}

const styles = {
    Header: Header,
    Info : Info,
    TextHeight : TextHeight
}

export default styles;