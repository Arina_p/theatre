import React, {Component} from 'react'
import "./repertoire.css"
import styles from "../about/styles";

class Repertoire extends Component {
    QUOTE_SERVICE_URL = 'http://localhost:8081/api/repertoire';

    constructor() {
        super();
        this.state = {isFetching: true, performances: null};
    }

    componentDidMount() {
        this.fetchQuotes();
    }

    //<PerformancesList performances={this.state.performances} />
    formatPerformance(performance){
          return  performance.authorName + "  \" " + performance.name + "\" " ;
    }
    render() {
        const title = 'Репертуар театра'
        console.log(this.state.performances);
        return (
            <div className='Repertoire'>
                <h2 style={styles.Header}>{title}</h2>
                <p style={styles.TextHeight}>{this.state.isFetching ? 'Fetching quotes...' : " "}</p>
                {this.state.performances && this.state.performances.map((performance) => {
                    return <p style={styles.Info}>{this.formatPerformance(performance)}</p>
                })}
            </div>

        );
    }

    fetchQuotes = () => {
        this.setState({isFetching: true})
        fetch(this.QUOTE_SERVICE_URL)
            .then(response => response.json())
            .then(result => {
                console.log("xyu", result);
                this.setState({performances: result,
                    isFetching: false});
            })
            .catch(e => console.log(e));
    }
}

export default Repertoire