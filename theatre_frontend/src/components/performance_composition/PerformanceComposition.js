import React, {Component} from 'react'
import "./perf_composition.css"
import "./styles.js"
import styles from "./styles";

class PerformanceComposition extends Component {
    QUOTE_SERVICE_PERFORMANCE_URL = 'http://localhost:8081/api/repertoire';
    QUOTE_SERVICE_ACTORS_URL = 'http://localhost:8081/api/actors?id=';
    QUOTE_SERVICE_PRODUCERS_URL = 'http://localhost:8081/api/producers?id=';

    constructor() {
        super();
        this.state = {performances: null, selectedPerformance: null, actors : null, producers : null};
    }

    componentDidMount() {
        this.fetchQuotes();
    }

    getScheduleForMentionedPerformance(e) {
        if(e.target.value == 0)
            this.state = {performances: null, selectedPerformance: null, actors : null, producers : null};

        fetch(this.QUOTE_SERVICE_ACTORS_URL + e.target.value)
            .then(response => response.json())
            .then(result => this.setState({actors: result}))
            .catch(e => console.log(e));

        fetch(this.QUOTE_SERVICE_PRODUCERS_URL + e.target.value)
            .then(response => response.json())
            .then(result => this.setState({producers: result}))
            .catch(e => console.log(e));
    }

    render() {
        return (
            <div className='Composition' style={styles.TextWeight}>
                <h2 > Актеры и постановщики </h2>
                <p style={styles.Info}> Выберите спектакль </p>
                <select onChange={this.getScheduleForMentionedPerformance.bind(this)}>
                    return <option value={0}>{"-"}</option>
                    {this.state.performances && this.state.performances.map((performance) => {
                        return <option key={performance.id} value={performance.id}>{performance.name}</option>})}
                </select>

                {this.state.actors && this.state.actors.length!=0 && <h5 style={styles.Subtitle} > Актерский состав: </h5>}
                {this.state.actors && this.state.actors.map((el) => {
                        return <p>{el}</p>;
                })}

                {this.state.producers && this.state.producers.length!=0 &&  <h5 style={styles.Subtitle}> Постановщики: </h5>}
                {this.state.producers && this.state.producers.map((el) =>{
                    console.log(el);
                    return <p>{el}</p>;
                })}
            </div>

        );
    }

    fetchQuotes = () => {
        this.setState({...this.state, isFetching: true})
        fetch(this.QUOTE_SERVICE_PERFORMANCE_URL)
            .then(response => response.json())
            .then(result => this.setState({performances: result}))
            .catch(e => console.log(e));
    }
}

export default PerformanceComposition