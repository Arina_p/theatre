const TextWeight = {
    position: "relative",
    left : "100px"
}

const Subtitle = {
    textColor: "black",
    fontWeight: 'bold',
    fontSize: "16px"
}

const Info = {
    textColor: "black",
    fontWeight: 'normal',
    fontSize: "16px"
}

const styles = {
    TextWeight : TextWeight,
    Info : Info,
    Subtitle :Subtitle
}

export default  styles;