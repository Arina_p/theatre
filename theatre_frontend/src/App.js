import React from 'react'
import { Link, Route, Switch} from 'react-router-dom'
import About from "./components/about/About";
import Repertoire from "./components/repertoire/Repertoire.js";
import Schedule from "./components/schedule/Schedule";
import PerformanceComposition from "./components/performance_composition/PerformanceComposition";


const Main = () => (
    <main>
      <Switch>
        <Route exact path='/' component={About}/>
        <Route path='/repertoire' component={Repertoire}/>
        <Route path='/schedule' component={Schedule}/>
        <Route path='/perf_composition' component={PerformanceComposition}/>
      </Switch>
    </main>
)

const Header = () => (
    <header>
      <nav>
        <ul>
          <li><Link to='/'>Главная страница</Link></li>
          <li><Link to='/repertoire'>Репертуар театра</Link></li>
          <li><Link to='/schedule'>Расписание спектаклей</Link></li>
          <li><Link to='/perf_composition'>Актеры и постановщики</Link></li>
        </ul>
      </nav>
    </header>
)

const App = () => (
    <div>
      <Header />
      <Main />
    </div>
)

export default App;