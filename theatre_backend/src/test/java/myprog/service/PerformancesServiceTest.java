package myprog.service;

import junit.framework.TestCase;
import myprog.entity.Actor;
import myprog.entity.Performance;
import myprog.entity.Producer;
import myprog.entity.Schedule;
import myprog.repository.ActorRepository;
import myprog.repository.PerformanceRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PerformanceRepository.class, PerformancesService.class, ActorRepository.class})
public class PerformancesServiceTest {
    @MockBean
    private ActorRepository actorRepository;
    @MockBean
    private PerformanceRepository performanceRepository;
    @Autowired
    private PerformancesService performancesService;

    Performance performance;

    @Before
    public void addTestPerformance() {
        performance = new Performance();
        performance.setAuthorName("Горький");
        performance.setName("Макар Чудра");

        Mockito.when(performanceRepository.findById(1)).thenReturn(Optional.of(performance));
        Mockito.when(performanceRepository.findById(2)).thenReturn(Optional.empty());
    }

    @Test
    public void successTestGetAuthor() { Assert.assertEquals("Горький", performancesService.getAuthor(1)); }

    @Test
    public void failTestGetAuthor() { Assert.assertEquals(null, performancesService.getAuthor(2)); }

    @Test
    public void successTestGetActorsList() {
        Actor actor1 = new Actor();
        Actor actor2 = new Actor();
        String name1 = "Муравьева";
        String name2 = "Семенов";

        actor1.setName(name1);
        actor2.setName(name2);
        actor1.setId(1);
        actor2.setId(2);

        Set<String> actorsNames = new HashSet<>();
        actorsNames.add(name1);
        actorsNames.add(name2);

        Set<Actor> actors = new HashSet<>();
        actors.add(actor1);
        actors.add(actor2);

        performance.setActors(actors);
        Assert.assertEquals(actorsNames, performancesService.getActorsList(1));
    }

    @Test
    public void failTestGetActorsList() {
        Assert.assertEquals(Collections.emptySet(), performancesService.getActorsList(2));
    }

    @Test
    public void successTestGetProducersList() {
        Producer producer1 = new Producer();
        Producer producer2 = new Producer();
        String name1 = "Воробьев";
        String name2 = "Санин";
        String rank1 = "заслуженный артист";

        producer1.setName(name1);
        producer1.setId(1);
        producer1.setCategory(rank1);

        producer2.setName(name2);
        producer2.setId(2);

        Set<Producer> producers = new HashSet<>();
        producers.add(producer1);
        producers.add(producer2);
        performance.setProducers(producers);

        Set<String> producersNames = new HashSet<>();
        producersNames.add(name1);
        producersNames.add(name2);

        Assert.assertEquals( producersNames, performancesService.getProducersList(1));
    }

    @Test
    public void failTestGetProducersList() {
        Assert.assertEquals( Collections.emptySet(), performancesService.getProducersList(2));
    }

    @Test
    public void successTestGetScheduleList() throws ParseException {
        Schedule schedule1 = new Schedule();
        Schedule schedule2 = new Schedule();
        Date data1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/05/27 19:55:00");
        Date data2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/05/29 19:55:00");
        schedule1.setSessionDateTime(data1);
        schedule2.setSessionDateTime(data2);
        schedule1.setId(1);
        schedule2.setId(2);

        Set<Schedule> schedules = new HashSet<>();
        schedules.add(schedule1);
        schedules.add(schedule2);
        performance.setSchedules(schedules);

        Set<String> dates = new HashSet<>();
        dates.add(data1.toString());
        dates.add(data2.toString());

        Assert.assertEquals(dates, performancesService.getScheduleList(1));
    }

    @Test
    public void failTestGetScheduleList(){
        Assert.assertEquals(Collections.emptySet(), performancesService.getScheduleList(2));
    }
}