package myprog.service;

import myprog.entity.Performance;
import myprog.entity.Schedule;
import myprog.repository.ActorRepository;
import myprog.repository.PerformanceRepository;
import myprog.repository.ScheduleRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PerformancesService.class,  ScheduleService.class})
public class ScheduleServiceTest {
    @MockBean
    private PerformanceRepository performanceRepository;
    @MockBean
    private ScheduleRepository scheduleRepository;
    @Autowired
    private ScheduleService scheduleService;

    Performance performance1, performance2, performance3;
    Set<String> sDates;
    Set<Schedule> dates;
    Date dateEndWeek, dateStartWeek;

    @Before
    public void addTestPerformance() throws ParseException {
        performance1 = new Performance();
        performance1.setAuthorName("Горький");
        performance1.setName("Макар Чудра");
        performance1.setId(1);

        performance2 = new Performance();
        performance2.setAuthorName("Бедная Лиза");
        performance2.setName("Карамзин");
        performance2.setId(2);

        performance3 = new Performance();
        performance3.setAuthorName("Снегурочка");
        performance3.setName("Островский");
        performance3.setId(3);

        Mockito.when(performanceRepository.findById(1)).thenReturn(Optional.of(performance1));
        Mockito.when(performanceRepository.findById(2)).thenReturn(Optional.of(performance2));

        Schedule schedule1 = new Schedule();
        Schedule schedule2 = new Schedule();
        Schedule schedule3 = new Schedule();
        Schedule schedule4 = new Schedule();
        Date data1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/05/27 19:55:00");
        Date data2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/05/29 19:55:00");
        Date data3 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/05/10 19:55:00");
        Date data4 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/06/30 19:55:00");
        schedule1.setSessionDateTime(data1);
        schedule2.setSessionDateTime(data2);
        schedule3.setSessionDateTime(data3);
        schedule4.setSessionDateTime(data4);
        schedule1.setId(1);
        schedule2.setId(2);
        schedule3.setId(3);
        schedule4.setId(4);

        Set<Schedule> schedules1 = new HashSet<>();
        schedules1.add(schedule1);
        schedules1.add(schedule2);
        schedules1.add(schedule3);
        schedules1.add(schedule4);
        performance1.setSchedules(schedules1);

        sDates = new HashSet<>();
        sDates.add(data1.toString());
        sDates.add(data2.toString());

        dates = new HashSet<>();
        dates.add(schedule1);
        dates.add(schedule2);

        Set<Schedule> schedules2 = new HashSet<>();
        schedules2.add(schedule3);
        schedules2.add(schedule4);
        performance2.setSchedules(schedules2);

        Set<Schedule> schedules3 = new HashSet<>();
        schedules3.add(schedule2);
        schedules3.add(schedule3);
        performance1.setSchedules(schedules3);
        
        dateEndWeek = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/05/27 19:55:00");
        dateStartWeek = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("2020/06/03 19:55:00");
        Mockito.when(scheduleRepository.getThisStartWeekDate()).thenReturn(dateStartWeek);
        Mockito.when(scheduleRepository.getThisEndWeekDate()).thenReturn(dateEndWeek);
    }

    @Test
    public void getPerformancesNamesOnNextSevenDays() throws ParseException {
       Set<Performance> performances = new HashSet<>();
       performances.add(performance1);
       performances.add(performance3);

       Mockito.when(scheduleRepository.findPerformancesDuringMentionedWeek(dateStartWeek, dateEndWeek)).thenReturn(performances);
       Assert.assertEquals(performances, scheduleService.getPerformancesNamesOnNextSevenDays());
    }

    @Test
    public void getAllScheduleOnNextSevenDaysForMentionedPerformance() {
        Mockito.when(scheduleRepository.findAllScheduleOnNextSevenDaysForMentionedPerformance(1, dateStartWeek, dateEndWeek))
                .thenReturn(dates);
        Assert.assertEquals(sDates, scheduleService.getAllScheduleOnNextSevenDaysForMentionedPerformance(1));
    }
}