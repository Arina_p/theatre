package myprog.controller;

import myprog.entity.Performance;
import myprog.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/api")
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;

    @GetMapping("/repertoire")
    public Set<Performance> getRepertoire(){
        return scheduleService.getPerformancesNamesOnNextSevenDays();
    }

    @GetMapping("/schedule")
    public Set<String> getSchedule(@Param("id") Integer id){
        return scheduleService.getAllScheduleOnNextSevenDaysForMentionedPerformance(id);
    }
}
