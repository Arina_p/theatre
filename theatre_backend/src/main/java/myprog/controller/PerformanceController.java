package myprog.controller;

import myprog.service.PerformancesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api")
public class PerformanceController {
    @Autowired
    private PerformancesService performancesService;

    @GetMapping("/actors")
    public Set<String> getActorsList(@Param("id") Integer id){
        return performancesService.getActorsList(id);
    }

    @GetMapping("/author")
    @ResponseBody
    public String getAuthor(@Param("id")  Integer id){
        return performancesService.getAuthor(id);
    }

    @GetMapping("/producers")
    @ResponseBody
    public Set<String> getProducersList(@Param("id") Integer id){
        return performancesService.getProducersList(id);
    }

    @GetMapping("/schedules")
    @ResponseBody
    public Set<String> getSchedulesList(@Param("id")    Integer id){
        return performancesService.getActorsList(id);
    }
}
