package myprog.service;

import myprog.entity.Actor;
import myprog.entity.Performance;
import myprog.entity.Producer;
import myprog.entity.Schedule;
import myprog.repository.PerformanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PerformancesService {

    @Autowired
    private PerformanceRepository performanceRepository;

    public Set<String> getActorsList(Integer id){
        Optional<Performance> performanceOptional = performanceRepository.findById(id);
        if (performanceOptional.isPresent()) {
            Performance performance = performanceOptional.get();
            Set<String> actorNames = new HashSet<>();
            for (Actor actor : performance.getActors())
                actorNames.add(actor.getName());
            return actorNames;
        }
        return Collections.emptySet();
    }

    public Set<String> getProducersList(Integer id) {
        Optional<Performance> performanceOptional = performanceRepository.findById(id);
        if (performanceOptional.isPresent()) {
            Performance performances = performanceOptional.get();
            Set<String> producersNames = new HashSet<>();
            for (Producer producer :performances.getProducers())
                producersNames.add(producer.getName());
            return producersNames;
        }
        return Collections.emptySet();
    }

    public Set<String> getScheduleList(Integer id){
        Optional<Performance> performanceOptional = performanceRepository.findById(id);
        if (performanceOptional.isPresent()) {
            Performance performance = performanceOptional.get();
            Set<String> scheduleSet = new HashSet<>();
            for (Schedule schedule :performance.getSchedules())
                scheduleSet.add(schedule.getSessionDateTime().toString());
            return scheduleSet;
        }
        return Collections.emptySet();
    }

    public String getAuthor(Integer id){
        Optional<Performance> performanceOpt = performanceRepository.findById(id);
        if (performanceOpt.isPresent()) {
            Performance performance = performanceOpt.get();
            return performance.getAuthorName();
        }
        return null;
    }

}
