package myprog.service;

import myprog.entity.Performance;
import myprog.entity.Schedule;
import myprog.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Service
public class ScheduleService {
    @Autowired
    private ScheduleRepository scheduleRepository = null;

    public Set<Performance> getPerformancesNamesOnNextSevenDays(){
        Date dataStartWeek = scheduleRepository.getThisStartWeekDate();
        Date dateEndWeek = scheduleRepository.getThisEndWeekDate();
        return scheduleRepository.findPerformancesDuringMentionedWeek(dataStartWeek, dateEndWeek);
    }

    public Set<String> getAllScheduleOnNextSevenDaysForMentionedPerformance(Integer performanceId){
        Date dataStartWeek = scheduleRepository.getThisStartWeekDate();
        Date dateEndWeek = scheduleRepository.getThisEndWeekDate();
        Set<Schedule> schedules =  scheduleRepository.findAllScheduleOnNextSevenDaysForMentionedPerformance(performanceId, dataStartWeek, dateEndWeek);
        Set<String> schedulesDataTime = new HashSet<>();
        for (Iterator<Schedule> it = schedules.iterator(); it.hasNext();)
            schedulesDataTime.add(it.next().getSessionDateTime().toString());
        return schedulesDataTime;
    }

}
