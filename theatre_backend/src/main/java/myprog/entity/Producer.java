package myprog.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "producers")
public class Producer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //autoincrement
    private Integer id;
    @Column
    String name;

    public void setName(String name) {
        this.name = name;
    }

    public Set<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(Set<Performance> performances) {
        this.performances = performances;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column
    String category;

    @ManyToMany
    private Set<Performance> performances;

    public String getName() {
        return name;
    }
}
