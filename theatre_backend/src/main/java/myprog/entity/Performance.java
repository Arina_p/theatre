package myprog.entity;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "performances")
public class Performance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //autoincrement
    private Integer id;
    @Column
    private String name;
    @Column
    private String authorName;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "performances")
    private Set<Actor> actors;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "performances")
    private Set<Schedule> schedules;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "performances")
    private Set<Producer> producers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Set<Actor> getActors() {
        return actors;
    }

    public void setActors(Set<Actor> actors) {
        this.actors = actors;
    }

    public Set<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(Set<Schedule> schedules) {
        this.schedules = schedules;
    }

    public Set<Producer> getProducers() {
        return producers;
    }

    public void setProducers(Set<Producer> producers) {
        this.producers = producers;
    }

    public String getAuthorName(){
        return authorName;
    }

    public void setAuthorName(String authorName){
        this.authorName = authorName;
    }

    public void setName(String name){
        this.name = name;
    }
}
