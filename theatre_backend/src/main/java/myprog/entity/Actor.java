package myprog.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "actors")
public class Actor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //autoincrement
    private Integer id;
    @Column
    String name;

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column
    String rank;

    @ManyToMany
    private Set<Performance> performances;

    public String getName() {
        return name;
    }
}
