package myprog.repository;

import myprog.entity.Performance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PerformanceRepository extends CrudRepository<Performance, Integer> {

}

