package myprog.repository;

import myprog.entity.Performance;
import myprog.entity.Schedule;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Set;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Integer> {
    @Query(value="SELECT current_date", nativeQuery=true)
    Date getThisStartWeekDate();

    @Query(value="SELECT current_date + 7", nativeQuery=true)
    Date getThisEndWeekDate();

    @Query("select p from Performance p where exists (select s from Schedule s join s.performances sp where s.sessionDateTime BETWEEN :startWeekDate AND :endWeekDate AND sp.id=p.id)")
    Set<Performance> findPerformancesDuringMentionedWeek(Date startWeekDate, Date endWeekDate);

    @Query("select s from Schedule s join s.performances sp where sp.id=:performanceId AND s.sessionDateTime BETWEEN :startWeekDate AND :endWeekDate")
    Set<Schedule> findAllScheduleOnNextSevenDaysForMentionedPerformance(Integer performanceId, Date startWeekDate, Date endWeekDate);
}
