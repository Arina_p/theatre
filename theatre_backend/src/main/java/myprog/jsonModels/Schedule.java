package myprog.jsonModels;

import java.util.Date;
import java.util.Set;

public class Schedule {
    private Integer id;
    Date sessionDateTime;

    public void setSessionDateTime(Date sessionDateTime) {
        this.sessionDateTime = sessionDateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getSessionDateTime(){
        return sessionDateTime;
    }
}
